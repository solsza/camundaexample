package so.requestAutoimpl.processeManager.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import so.requestAutoimpl.processeManager.dto.ProcessInstanceDTO;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProcessInstanceDtoMapper {

    public static List<ProcessInstanceDTO> mapToDtos(List<ProcessInstance> processInstances) {
        return processInstances.stream()
                .map(processInstance -> mapToDto(processInstance))
                .collect(Collectors.toList());
    }

    public static ProcessInstanceDTO mapToDto(ProcessInstance processInstance) {
        return ProcessInstanceDTO.builder()
                .instanceId(processInstance.getId())
                .instanceName(processInstance.getProcessDefinitionId())
                .processKey(processInstance.getBusinessKey())
                .build();
    }
}
