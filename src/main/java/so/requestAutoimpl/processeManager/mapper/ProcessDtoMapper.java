package so.requestAutoimpl.processeManager.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import so.requestAutoimpl.processeManager.dto.ProcessDto;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProcessDtoMapper {

    public static List<ProcessDto> mapToDtos(List<ProcessDefinition> processes) {
        return processes.stream()
                .map(process -> mapToDto(process))
                .collect(Collectors.toList());
    }

    public static ProcessDto mapToDto(ProcessDefinition process) {
        return ProcessDto.builder()
                .processId(process.getId())
                .processName(process.getKey())
                .processVersion(String.valueOf(process.getVersion()))
                .build();
    }
}