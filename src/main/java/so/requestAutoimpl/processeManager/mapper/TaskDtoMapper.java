package so.requestAutoimpl.processeManager.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.camunda.bpm.engine.task.Task;
import so.requestAutoimpl.processeManager.dto.TaskDto;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TaskDtoMapper {

    public static List<TaskDto> mapToDtos(List<Task> tasks) {
        return tasks.stream()
                .map(task -> mapToDto(task))
                .collect(Collectors.toList());
    }

    public static TaskDto mapToDto(Task task) {
        return TaskDto.builder()
                .taskId(task.getId())
                .taskName(task.getName())
                .build();
    }
}
