package so.requestAutoimpl.processeManager.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import so.requestAutoimpl.processeManager.dto.ProcessDto;
import so.requestAutoimpl.processeManager.dto.ProcessInstanceDTO;
import so.requestAutoimpl.processeManager.dto.TaskDto;
import so.requestAutoimpl.processeManager.mapper.ProcessDtoMapper;
import so.requestAutoimpl.processeManager.mapper.ProcessInstanceDtoMapper;
import so.requestAutoimpl.processeManager.mapper.TaskDtoMapper;
import so.requestAutoimpl.processeManager.service.ProcessService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/processes")
@RequiredArgsConstructor
public class ProcessController {

    private final ProcessService processService;

    // list of all processes latest version
    @GetMapping("/getProcesses")
    public List<ProcessDto> getProcesses() {
        return ProcessDtoMapper.mapToDtos(processService.getProcesses());
    }

    // list of all active instance for process (processName)
    @GetMapping("/getActiveInstancesOfProcess/{processKey}")
    public List<ProcessInstanceDTO> getActiveInstancesOfProcesses(@PathVariable String processKey) {
        return ProcessInstanceDtoMapper.mapToDtos(processService.getAllRunningInstancesForProcess(processKey));
    }

    // list of variables for instance of process
    @GetMapping("/getProcessInstanceVariables/{instanceId}")
    public Map<String, Object> getInstanceVariables(@PathVariable String instanceId) {
        return processService.getVariablesForProcess(instanceId);
    }

    //list of users-tasks for process instance id
    @GetMapping("/getTaskList/{instanceId}")
    public List<TaskDto> getTaskList(@PathVariable String instanceId) {
        return TaskDtoMapper.mapToDtos(processService.getTaskForProcessInstance(instanceId));
    }

    //listOfProcessFormVariables
    @GetMapping("/getFormVariablesToStartProcess/{processId}")
    public Map<String, Object> getProcessFormVariables(@PathVariable String processId) {
        return processService.getProcessFormVariables(processId);
    }

    //listOfTaskVariables
    @GetMapping("/getFormVariablesToCompleteTask/{taskId}")
    public Map<String, Object> getTaskFormVariables(@PathVariable String taskId) {
        return processService.getTaskFormVariables(taskId);
    }


    @PostMapping("/{processKey}/startProcessInstance")
    public ProcessInstanceDTO startProcessInstance(@RequestBody Map<String, Object> properties, @PathVariable String processKey) {
        // return instance parameters
        return ProcessInstanceDtoMapper.mapToDto(processService.startProcessInstance(processKey, properties));
    }

    @PostMapping("/{processId}/{businessKey}/startProcessInstanceWithDefaultForm")
    public ProcessInstanceDTO startProcessInstanceWithDefaultForm(@RequestBody Map<String, Object> properties, @PathVariable String processId, @PathVariable String businessKey) {
        // return instance parameters
        return ProcessInstanceDtoMapper.mapToDto(processService.startProcessInstanceWithDefaultForm(processId, properties, businessKey));
    }

    //set variables of process in runtime
    @PostMapping("/setVariablesOfProcessInstance/{instanceId}")
    public Map<String, Object> setVariablesForProcessInstance(@PathVariable String instanceId, @RequestBody Map<String, Object> mapOfVariables) {
        return processService.setVariablesForProcessInstance(instanceId, mapOfVariables);
    }

    //accept user task with variables
    @PostMapping("/acceptUserTaskWithVariables/{taskId}")
    public TaskDto acceptUserTaskWithVariables(@PathVariable String taskId, @RequestBody Map<String, Object> mapOfVariables) {
        return TaskDtoMapper.mapToDto(processService.acceptUserTaskWithVariables(taskId, mapOfVariables));
    }

    //send message
    @PostMapping("{processId}/sendMessage/{message}")
    public void sendMessage(@PathVariable String processId, @PathVariable String message) {
        processService.sendMessage(processId, message);
    }

    //deleteProcessInstance
    @DeleteMapping("/instances/{instanceId}")
    public void deleteInstances(@PathVariable String instanceId, @RequestBody String reason) {
        processService.deleteInstances(instanceId, reason);
    }

    //deleteProcessWithInstances
    @DeleteMapping("/{processKey}")
    public void deleteProcess(@PathVariable String processKey) {
        processService.deleteProcess(processKey);
    }


}
