package so.requestAutoimpl.processeManager.service;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.variable.VariableMap;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProcessService {


    private final ProcessEngine processEngine;
    private final RuntimeService runtimeService;
    private final RepositoryService repositoryService;


    public List<ProcessDefinition> getProcesses() {
        // get list of active process . latest version
        List<ProcessDefinition> processDefinition = repositoryService.createProcessDefinitionQuery().active().latestVersion().list();
        return processDefinition;
    }

    public List<ProcessInstance> getAllRunningInstancesForProcess(String processKey) {

        //get latest process definition for process (processName)
        ProcessDefinition processDefinition =
                repositoryService.createProcessDefinitionQuery()
                        .processDefinitionKey(processKey)
                        .latestVersion()
                        .singleResult();

        //list for all active instance of active process, latest version (processName)
        List<ProcessInstance> processInstances =
                runtimeService.createProcessInstanceQuery()
                        .processDefinitionId(processDefinition.getId())
                        .active()
                        .list();
        return processInstances;
    }

    public Map<String, Object> getVariablesForProcess(String instanceId) {
        //get values for instance of the process
        Map<String, Object> values = runtimeService.getVariables(instanceId);
        return values;
    }

    public List<Task> getTaskForProcessInstance(String instanceId) {
        //get list of task for process key
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().executionId(instanceId).list();

        return tasks;
    }

    public Map<String, Object> getProcessFormVariables(String processID) {

        FormService formService = processEngine.getFormService();
        return formService.getStartFormVariables(processID);
    }

    public Map<String, Object> getTaskFormVariables(String taskId) {
        FormService formService = processEngine.getFormService();
        VariableMap variableMap = formService.getTaskFormVariables(taskId);

        List<String> list = getFormFieldName(taskId, formService);
        Map<String, Object> mapOfFormVariables = new HashMap<>();
        for (String field : list
        ) {
            if (variableMap.containsKey(field)) {
                mapOfFormVariables.put(field, variableMap.get(field));
            }
        }

        return mapOfFormVariables;
    }

    private List<String> getFormFieldName(String taskId, FormService formService) {
        TaskFormData taskFormData = formService.getTaskFormData(taskId);
        return taskFormData.getFormFields().stream().map(formField -> formField.getId()).collect(Collectors.toList());
    }

    public ProcessInstance startProcessInstance(String processKey, Map<String, Object> properties) {

        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processKey, properties);
        return processInstance;
    }

    public ProcessInstance startProcessInstanceWithDefaultForm(String processDefinitionId, Map<String, Object> properties, String businessKey) {

        FormService formService = processEngine.getFormService();
        return formService.submitStartForm(processDefinitionId, businessKey, properties);

    }

    public Map<String, Object> setVariablesForProcessInstance(String instanceId, Map<String, Object> mapOfVariables) {
        //set values for instance of the process
        runtimeService.setVariables(instanceId, mapOfVariables);
        return runtimeService.getVariables(instanceId);
    }

    public Task acceptUserTaskWithVariables(String taskId, Map<String, Object> mapOfVariables) {
        Task task = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
        processEngine.getTaskService().complete(taskId, mapOfVariables);
        return task;
    }

    public String deleteInstances(String instanceId, String reason) {
        runtimeService.deleteProcessInstance(instanceId, reason);
        return reason;
    }

    public void deleteProcess(String processKey) {

        repositoryService.deleteProcessDefinitions().byKey(processKey).cascade().delete();

    }

    public void sendMessage(String instanceId, String message) {
        runtimeService.createMessageCorrelation(message).processInstanceId(instanceId).correlateAllWithResult();
    }



}
