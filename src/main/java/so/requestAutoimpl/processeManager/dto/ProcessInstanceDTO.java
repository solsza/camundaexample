package so.requestAutoimpl.processeManager.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProcessInstanceDTO {

    private String instanceId;
    private String instanceName;
    private String processKey;
}
