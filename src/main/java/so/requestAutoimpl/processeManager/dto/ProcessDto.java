package so.requestAutoimpl.processeManager.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProcessDto {

    private String processId;
    private String processName;
    private String processVersion;
}