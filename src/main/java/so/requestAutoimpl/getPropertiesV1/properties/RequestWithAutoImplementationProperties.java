package so.requestAutoimpl.getPropertiesV1.properties;

import org.springframework.stereotype.Component;

@Component
public class RequestWithAutoImplementationProperties {

    public static final boolean createdFromReview = false;
    public static final boolean validationNeeded = false;
    public static final boolean auto = false;
    public static final String setApproverOnValidationClass = "setApproverOnValidation";
    public static final String setApproverOnDelegationClass = "setApproverOnDelegation";
    public static final String queuingClass = "queuing";
    public static final String createModifyATARsClass ="createModifyATARs";

    public RequestWithAutoImplementationProperties() {
    }
}
