package so.requestAutoimpl.getPropertiesV1.service;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PropertiesService {

    private final ApplicationContext applicationContext;

    public Map<String, Object> getWorkFlowProperties(String workFlowDefinitionKey) {

        String beanName = workFlowDefinitionKey.substring(0,1).toLowerCase() + workFlowDefinitionKey.substring(1) + "Properties";
        Object object = applicationContext.getBean(beanName);

        return getMapFromObject(object);
    }

    private Map<String,Object> getMapFromObject(Object object) {
        Field[] fields = object.getClass().getFields();
        Map<String, Object> map = new HashMap<String, Object>();
        for(Field field : fields) {
            try {
                map.put(field.getName(),field.get(field));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }
}
