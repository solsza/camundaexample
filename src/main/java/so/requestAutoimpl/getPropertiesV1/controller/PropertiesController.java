package so.requestAutoimpl.getPropertiesV1.controller;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import so.requestAutoimpl.getPropertiesV1.service.PropertiesService;
import java.util.Map;

@RestController
@RequestMapping("/properties")
@RequiredArgsConstructor
public class PropertiesController {

    private final PropertiesService propertiesService;

    @GetMapping("/{workFlowDefinitionKey}/workflow")
    public Map<String, Object> getWorkFlowProperties(@PathVariable String workFlowDefinitionKey) {
        return propertiesService.getWorkFlowProperties(workFlowDefinitionKey);
    }
}
