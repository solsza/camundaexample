package so.requestAutoimpl.deploy.validator;

import lombok.extern.log4j.Log4j2;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.regex.Pattern;

/*
File validator base solution, to improve
 */
@Log4j2
public class FileValidator {

    public static Boolean fileNameValidation(MultipartFile file) {
        String fileNameTMP = StringUtils.cleanPath(file.getOriginalFilename());
        final Pattern pattern = Pattern.compile("^[a-z0-9_@-]+(\\.bpmn|\\.BPMN)$");
        if (!pattern.matcher(fileNameTMP).matches()) {
            log.info("Filename " + fileNameTMP + " is incorrect");
            return true;
        }
        return false;
    }
}
