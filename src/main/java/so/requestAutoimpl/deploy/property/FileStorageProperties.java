package so.requestAutoimpl.deploy.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

/*
    Properties for upload BPMN file
 */
@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {

    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }
}
