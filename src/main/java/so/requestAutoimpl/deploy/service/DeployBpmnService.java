package so.requestAutoimpl.deploy.service;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.repository.Deployment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class DeployBpmnService {

    private final ProcessEngine camunda;

    public String deployBpmn(String fileName, InputStream inputStream) {

        String deploymentName = "New Deployment " + LocalDateTime.now();

        Deployment deployment = camunda.getRepositoryService()
                .createDeployment()
                .name(deploymentName)
                .addInputStream(fileName, inputStream)
                .deploy();

        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return deployment.getId();
    }
}