package so.requestAutoimpl.deploy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import so.requestAutoimpl.deploy.exception.FileStorageException;
import so.requestAutoimpl.deploy.property.FileStorageProperties;
import so.requestAutoimpl.deploy.validator.FileValidator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/*
For deployment file in class path and deployment file to camunda
 */
@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    private final DeployBpmnService deployBpmnService;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties, DeployBpmnService deployBpmnService) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the storage directory", ex);
        }
        this.deployBpmnService = deployBpmnService;


    }

    public String storeFile(MultipartFile file) {

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's by regex
            if (FileValidator.fileNameValidation(file)) {
                throw new FileStorageException("Filename contains invalid path sequence " + fileName);
            }
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            deployBpmnService.deployBpmn(fileName, file.getInputStream());

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName, ex);
        }
    }
}