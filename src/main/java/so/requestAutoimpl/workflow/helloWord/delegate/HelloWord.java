package so.requestAutoimpl.workflow.helloWord.delegate;

import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Log4j2
@Component
public class HelloWord implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info(execution.getVariable("Message"));
        Map<String, Object> variables = execution.getVariablesLocal();

        for (Map.Entry<String, Object> entry : variables.entrySet()) {
            log.info(entry.getKey() + "\t");
            log.info(entry.getValue() + "\n");
        }
    }
}
