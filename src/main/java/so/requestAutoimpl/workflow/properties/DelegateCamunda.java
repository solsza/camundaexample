package so.requestAutoimpl.workflow.properties;

import org.camunda.bpm.engine.delegate.DelegateExecution;

public interface DelegateCamunda {

    void delegateWork (DelegateExecution execution);
}
