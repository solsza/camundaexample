package so.requestAutoimpl.workflow.RequestWithAutoImplementation.externalDelegate;

import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import so.requestAutoimpl.workflow.properties.DelegateCamunda;

import java.util.Map;

@Component
@Log4j2
public class SetApproverOnValidationExternal implements DelegateCamunda {
    @Override
    public void delegateWork(DelegateExecution execution) {

        log.info("I am in  external  delegate");
        log.info("I do some work");
        Map<String, Object> processVariables = execution.getVariables();
        processVariables.entrySet().forEach(variable -> {
            log.info(variable.getKey() + "\t" + variable.getValue() + "\n");
        });
    }
}
