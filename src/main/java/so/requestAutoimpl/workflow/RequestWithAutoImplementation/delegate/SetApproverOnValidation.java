package so.requestAutoimpl.workflow.RequestWithAutoImplementation.delegate;

import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import so.requestAutoimpl.getPropertiesV1.properties.RequestWithAutoImplementationProperties;
import so.requestAutoimpl.workflow.properties.DelegateCamunda;

import java.util.Map;

@Component
@Log4j2
public class SetApproverOnValidation implements JavaDelegate {

    @Autowired
    private ApplicationContext applicationContext;
    private String delegateValue = RequestWithAutoImplementationProperties.setApproverOnValidationClass;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        log.info("I am in SetApproverOnValidation Class");


        String setApproverOnValidationClass = (String) delegateExecution.getVariable("setApproverOnValidationClass");
        if (setApproverOnValidationClass.equals(delegateValue)) {
            log.info("I am in default delegate");
            log.info("I do some work");
            Map<String, Object> processVariables = delegateExecution.getVariables();
            processVariables.entrySet().forEach(variable -> {
                log.info(variable.getKey() + "\t" + variable.getValue() + "\n");
            });
        } else {
            DelegateCamunda delegateCamunda = (DelegateCamunda) applicationContext.getBean(setApproverOnValidationClass);
            delegateCamunda.delegateWork(delegateExecution);
        }
    }
}
