package so.requestAutoimpl.workflow.RequestWithAutoImplementation.delegate;

import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ThrowMessage  implements JavaDelegate {

   @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

       delegateExecution.getProcessEngine().getRuntimeService().createMessageCorrelation("Access request for review created").correlateAllWithResult();

    }
}
